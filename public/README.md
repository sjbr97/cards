# Adding games and backgrounds

## How to import games and backgrounds

Backgrounds and games have an ID. The folder in which the game or
background is located ends with this ID.

Backgrounds are located in the ```/public/gameBackgrounds/``` folder.
Their folder name is ```background{id}``` - {id} is their respective id.

Games are located in the ```/public/games/``` folder.
Your folder is named ```game{id}``` - {id} is their respective id.

The entrypoint for games and backgrounds is the ```index.html```.

After you put all the files into the folder, you need to create a preview.
The preview is an image with the size 512x288. Put it in the folder as well.

### Configure games and backgrounds

After you have added the folder for the background or the game, you can
enable it by adding an entry to the config (located in
```/imports/config/lockScreen.js```).

Hinzufügen von Spielen und Hintergründen #

## So importieren Sie Spiele und Hintergründe

Hintergründe und Spiele haben einen Ausweis. Der Ordner, in dem das Spiel oder
Der Hintergrund befindet sich und endet mit dieser ID.

`` Hintergründe befinden sich im Ordner / public / gameBackgrounds / ``.
Ihr Ordnername `` `` `` Hintergrund {it} - {} ist der jeweilige it.

`` Spiele befinden sich im Ordner / public / games / ``.
Dein Spielordner heißt `` `` `` {it} - {} ist das jeweilige it.

Der Einstiegspunkt für Spiele und Hintergründe ist der `` index.html```.

Nachdem Sie alle Dateien in den Ordner gelegt haben, müssen Sie eine Vorschau erstellen.
Die Vorschau ist ein Bild mit der Größe 512x288. Legen Sie es auch in den Ordner.

Konfigurieren Sie ### Spiele und Hintergründe

Nachdem Sie den Ordner für den Hintergrund oder das Spiel hinzugefügt haben, können Sie
Aktivieren Sie es, indem Sie der Konfiguration einen Eintrag hinzufügen (befindet sich in
`` / Imports / config / lockScreen.js```).

Konfiguration gibt es mehrere Möglichkeiten:
```
{
	"Id": 1, // Die ID im öffentlichen Ordner oder die gameID backgroundID
	"Name": {// Der Name des Spiels
		"Von" "bleibt in der Luft"
		"Sehen Sie" "Bleiben Sie in der Luft"
	}
	"ClockPosition" "top_right", // Position der Uhr, wenn nicht top_right, kann folgen (oben | unten) _ (rechts | links)
	"Hintergrund" "# 212529", // Hintergrund, wenn nicht Standardhintergrundfarbe
	"Vorschau": "Vorschau.png", // Der Pfad zur Vorschau des Bildes nach / games / game $ {id} / oder / gameBackgrounds / background $ {id} /, falls nicht / games / game $ {id} / Vorschau png oder /gameBackgrounds/background${id}/preview.png
	"Eigenschaften": {
		"Safari": true, // Für Safari aktivieren
		"Mobile": true // auf dem Handy aktivieren
	}
	"MaxWidth": {// Optionale Werte, nur Spiele
		"Desktop": 700, // Größe ist eine Desktop-Fensterbreite> = 768 Pixel (3 x Bootstrap-Haltepunkt)
		"Mobil": {// Mobilgröße ist eine Fensterbreite <768 Pixel (3 x Bootstrap-Haltepunkt)
			"Porträt": 500;
			"Landschaft": 400
		}
	}
	"MaxHeight": {// Optionale Werte, nur Spiele
		"Desktop": 700, // Höhe Fenstergröße ist ein Desktop
		"Mobil": {// Mobilgröße ist eine Fensterhöhe
			"Porträt": 500;
			"Landschaft": 400
		}
	}
}
```

Hinweis für ## Projekte CodePen

Sie können CodePen-Projekte exportieren, indem Sie unten rechts klicken
auf die 'Export'-Schaltfläche. Nachdem Sie heruntergeladen und extrahiert haben
Stellen Sie im Archiv sicher, dass Sie die Lizenz und das `` dist``` importiert haben
Ordner (falls vorhanden) in das entsprechende Verzeichnis.